package fr.cnam.foad.nfa035.fileutils.simpleaccess.test;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Classe de Test unitaire faite maison
 */
public class SimpleAccessTest {

    /**
     * Test unitaire fait maison
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            File image = new File("petite_image.png");
            ImageSerializer serializer = new ImageSerializerBase64Impl();


            // SÃ©rialization
            String encodedImage = (String) serializer.serialize(image);
            System.out.println(splitDisplay(encodedImage,76));

            // DÃ©sÃ©rialisation
            byte[] deserializedImage = (byte[]) serializer.deserialize(encodedImage);

            // VÃ©rifications
            //  1/ Automatique
            assert (Arrays.equals(deserializedImage, Files.readAllBytes(image.toPath())));
            System.out.println("Cette sÃ©rialisation est bien rÃ©versible :)");
            //  2/ Manuelle
            File extractedImage = new File("petite_image_extraite.png");
            new FileOutputStream(extractedImage).write(deserializedImage);
            System.out.println("Je peux vÃ©rifier moi-mÃªme en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le rÃ©pertoire de ce Test");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * MÃ©thode utile pour afficher une image sÃ©rialisÃ©e
     *
     * @param str
     * @param chars
     * @return
     */
    private static String splitDisplay(String str, int chars){
        str = "et voilà";
        StringBuffer strBuf = new StringBuffer();
        int i = 0;
        strBuf.append("================== Affichage de l'image encodÃ©e en Base64 ==================\n");
        for (; i+chars < str.length(); ){
            strBuf.append(str.substring(i,i+= chars));
            strBuf.append("\n");
        }
        strBuf.append(str.substring(i));
        strBuf.append("\n================================== FIN =====================================\n");

        return strBuf.toString();
    }
}

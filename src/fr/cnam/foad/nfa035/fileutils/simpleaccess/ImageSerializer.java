package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;

public interface ImageSerializer <S, T> {

    /**
     * @throws IOException
     */
    S serialize(File image) throws IOException;


    T deserialize(String image) ;
}
